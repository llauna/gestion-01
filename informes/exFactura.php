<?php
ob_start();
if (strlen(session_id()) < 1)
    session_start();

if (!isset($_SESSION["nombre"])) {
    echo 'Debe ingresar al sistema correctamente';
}
else {
    if ($_SESSION['ventas']==1) {

        require ('Factura.php');

        $logo = "logo.jpg";
        $ext_logo = "jpg";
        $empresa = "David Solanes Gavilán";
        $documento = "43399649Y";
        $direccion = "C/Concepción Arenal, 1";
        $telefono = "34658811991";
        $email = "davidsolanesgavilan@gmail.com";

        //datos de la cabecera
        require_once "../modelos/Venta.php";
        $venta = new Venta();
        $rsptav = $venta ->ventacabecera($_GET["id"]);
        //recorremos los valores obtenidos
        $regv = $rsptav->fetch_object();

        //configuración de la factura
        $pdf = new PDF_Invoice('P','mm','A4');
        $pdf->AddPage();

        //envio datos empresa al metodo addSociete
        $pdf->addSociete(utf8_decode($empresa), $documento."\n".
                                            utf8_decode("Dirección: ").utf8_decode($direccion)."\n".
                                            utf8_decode("Teléfono:  ").$telefono."\n".
                                            "Email :    ".$email,$logo,$ext_logo);
        $pdf->fact_dev("$regv->tipo_comprobante ", "$regv->serie_comprobante-$regv->num_comprobante");
        $pdf->temporaire("");
        $pdf->addDate($regv->fecha);
        //envio datos cliente al metodo addClientAdresse
        $pdf->addClientAdresse(utf8_decode($regv->cliente),"Domicilio: ".utf8_decode($regv->direccion),
            $regv->tipo_documento.": ".$regv->num_documento,"Email:  ".$regv->email,"Telefono: ".$regv->telefono);

        //Establecemos las columnas, donde mostramos loa detalles de la venta
        $cols=array( "CODIGO"=>23,
                     "DESCRIPCION"=>78,
                     "CANTIDAD"=>22,
                     "p/Unit"=>25,
                     "DTO."=>20,
                     "SUBTOTAL"=>22);
        $pdf->addCols($cols);
        $cols=array( "CODIGO"=>"L", //"L" left
                     "DESCRIPCION"=>"L",
                     "CANTIDAD"=>"C",
                     "p/Unit"=>"R",
                     "DTO."=>"R",
                     "SUBTOTAL"=>"C" );
        $pdf->addLineFormat($cols);
        $pdf->addLineFormat($cols);
        //ubicación desde donde empezaremos a mostrar los datos
        $y = 89;
        //obtenemos todos los detalles de la venta actual
        $rsptad = $venta->ventadetalle($_GET["id"]);

        while ($regd = $rsptad->fetch_object()) {
            $line = array(  "CODIGO"=>"$regd->codigo",
                            "DESCRIPCION"=>utf8_decode("$regd->articulo"),
                            "CANTIDAD"=>"$regd->cantidad",
                            "p/Unit"=>"$regd->precio_venta",
                            "DTO."=>"$regd->descuento",
                            "SUBTOTAL"=>"$regd->subtotal" );
                    $size = $pdf->addLine($y, $line);
                    $y += $size + 2;
        }

        //convertir el total en letras
        require_once "Letras.php";
        $V = new EnLetras();
        $con_letra = strtoupper($V->ValorEnLetras($regv->total_venta,"Euros"));
        $pdf->addCadreTVAs("---".$con_letra);

        //mostramos el impuesto
        $aux = chr(128);
        $pdf->addTVAs($regv->impuesto, $regv->total_venta, "$aux/ ");
        $pdf->addCadreEurosFrancs("I.V.A."." $regv->impuesto %"); //revisar !!!!
        $pdf->Output('Reporte de Venta', 'I');
    }
    else {
        echo 'No tiene permiso';
    }
}
ob_end_flush();
