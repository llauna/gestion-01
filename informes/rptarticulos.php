<?php
ob_start();
if (strlen(session_id()) < 1)
    session_start();
if (!isset($_SESSION["nombre"])) {
    echo 'Debe ingresar al sistema correctamente';
}
else {
if ($_SESSION['almacen']==1) {

require ('PDF_MC_Table.php');

$pdf=new PDF_MC_Table();
//primera pagina del documento
$pdf->AddPage();
//margen superior
$y_axis_initial = 25;
//tipo letra y titulo
$pdf->SetFont('Arial','B',12);
//detalle texto
$pdf->Cell(40,6,'',0,0,'C');
$pdf->Cell(100,6,'LISTA DE ARTICULOS',1,0,'C');
$pdf->Ln(10); //rectangulo para el titulo

$pdf->SetFillColor(232,232,232); //fondo
$pdf->SetFont('Arial','B',10);
$pdf->Cell(58,6,'Nombre',1,0,'C',1); //celda del documento
$pdf->Cell(50,6,utf8_decode('Categoría'),1,0,'C',1); //utilizamos utf8_decode para que reconozca el acento
$pdf->Cell(35,6,utf8_decode('Código'),1,0,'C',1);
$pdf->Cell(12,6, 'Stock',1,0,'C',1);
$pdf->Cell(35,6,utf8_decode('Descripción'),1,0,'C',1);
$pdf->Ln(10);

require_once "../modelos/Articulo.php";
$articulo = new Articulo();

$pdf->SetWidths(array(58,50,35,12,35));

$rspta = $articulo->listar();
while ($reg = $rspta->fetch_object()) {
    $nombre = $reg->nombre;
    $categoria = $reg->categoria;
    $codigo = $reg->codigo;
    $stock = $reg->stock;
    $descripcion = $reg->descripcion;

    $pdf->SetFont('Arial','',10);
    $pdf->Row(array(utf8_decode($nombre),utf8_decode($categoria),$codigo,$stock, utf8_decode($descripcion)));
}
$pdf->Output();
}
else {
    echo 'No tiene permiso';
}
}
ob_end_flush();
