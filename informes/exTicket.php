<?php
ob_start();
if (strlen(session_id()) < 1)
    session_start();

if (!isset($_SESSION["nombre"]))
{
    echo 'Debe ingresar al sistema correctamente para visualizar el reporte';
}
else
{
    if ($_SESSION['ventas']==1)
    {

    ?>
     <html lang="en">
     <head>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
         <link href="css/ticket.css">
         <title></title>
     </head>
     <body onload="window.print();">
     <?php
        require_once "../modelos/Venta.php";
        $venta = new Venta();

        $rspta = $venta->ventacabecera($_GET["id"]);

        $reg = $rspta->fetch_object();

        $empresa = "David Solanes Gavilán";
        $documento = "43399649Y";
        $direccion = "C/Concepción Arenal, 1";
        $telefono = "34658811991";
        $email = "davidsolanesgavilan@gmail.com";
     ?>
     <div class="zona_impresion">
         <!-- código ha imprimir -->
         <br>
         <table style="border: 0; text-align:center; width:300px;">
             <tr>
                 <td style="text-align:center;">
                     <!-- Mostramos los datos de la empresa en el documento HTML -->
                     .::<strong> <?php echo $empresa; ?></strong>::.<br>
                     <?php echo $documento; ?><br>
                     <?php echo $direccion .' - '.$telefono; ?><br>
                 </td>
             </tr>
             <tr>
                 <td style="text-align:right;">Fecha: <?php echo $reg->fecha; ?></td>
             </tr>
             <tr>
                 <td style="text-align:left;"></td>
             </tr>
             <tr>
                 <!-- Mostramos los datos del cliente en el documento HTML -->
                 <td>Cliente: <?php echo $reg->cliente; ?></td>
             </tr>
             <tr>
                 <td><?php echo $reg->tipo_documento.": ".$reg->num_documento; ?></td>
             </tr>
             <tr>
                 <td>Nº de venta: <?php echo $reg->serie_comprobante." - ".$reg->num_comprobante ; ?></td>
             </tr>
         </table>
         <br>
         <!-- Mostramos los detalles de la venta en el documento HTML -->
         <table style="border:1px; text-align:center; width:300px;">
             <tr>
                 <td>CANT.</td>
                 <td>DESCRIPCIÓN</td>
                 <td style="text-align:right;">IMPORTE</td>
             </tr>
             <tr>
                 <td colspan="3">==========================================</td>
             </tr>
             <?php
             $rsptad = $venta->ventadetalle($_GET["id"]);
             $cantidad=0;
             while ($regd = $rsptad->fetch_object()) {
                 echo "<tr>";
                 echo "<td>".$regd->cantidad."</td>";
                 echo "<td>".$regd->articulo;
                 echo "<td align='right'>€/ ".$regd->subtotal."</td>";
                 echo "</tr>";
                 $cantidad+=$regd->cantidad;
             }
             ?>
             <!-- Mostramos los totales de la venta en el documento HTML -->
             <tr>
                 <td>&nbsp;</td>
                 <td style="text-align:right;"><b>TOTAL:</b></td>
                 <td style="text-align:right;"><b>€/  <?php echo $reg->total_venta;  ?></b></td>
             </tr>
             <tr><td colspan="3">&nbsp;</td></tr>
             <tr>
                 <td colspan="3">Nº de artículos: <?php echo $cantidad; ?></td>
             </tr>
             <tr>
                 <td colspan="3">&nbsp;</td>
             </tr>
             <tr>
                 <td colspan="3" style="text-align:center;">¡Gracias por su compra!</td>
             </tr>
             <tr>
                 <td colspan="3" style="text-align:center;">Llauna</td>
             </tr>
             <tr>
                 <td colspan="3" style="text-align:center;"> Barcelona - Catalunya</td>
             </tr>
         </table>
         <br>
     </div>
     <p>&nbsp;</p>
     </body>
     </html>
        <?php
    }
    else
    {
        echo 'No tiene permiso para visualizar el reporte';
    }

}
ob_end_flush();
?>
