<?php
/**
 * Created by PhpStorm.
 * User: davidsolanes
 * Date: 27/7/18
 * Time: 21:00
 */

//Ip del servidor de base de datos
define ("DB_HOST", "127.0.0.1");

//Nombre de la base de datos
define("DB_NAME", "dbsistema");

//Usuario de la base de datos
define("DB_USERNAME", "root");

//Contraseña del usuario de la base da datos
define("DB_PASSWORD", "d4v1d001");

//definimos la codificación de los caracteres
define("DB_ENCODE", "utf8");

//Definimos una constante como nombre del proyecto
define("PRO_NOMBRE", "Financial");