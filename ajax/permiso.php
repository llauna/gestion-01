<?php
/**
 * Created by PhpStorm.
 * User: davidsolanes
 * Date: 28/7/18
 * Time: 22:35
 */

require_once "../modelos/Permiso.php";

$permiso=new Permiso();

switch ($_GET["op"]) {

    case 'listar':
        $rspta = $permiso->listar();
        $data = Array();
        while ($reg=$rspta->fetch_object()) {
            $data[]=array (
                "0"=>$reg->nombre,
            );
        }
        $results = array (
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDisplayRecords"=>count($data),
            "aaData"=>$data
        );
        echo json_encode($results);
        break;
}