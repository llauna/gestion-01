let tabla;

function init() {
    mostrarform(false);
    listar();

    $("#formulario").on("submit", function(e) {
        guardaryeditar(e);
    });
    $.post("../ajax/ingreso.php?op=selectProveedor", function (r) {
        $("#idproveedor").html(r);
        $('#idproveedor').selectpicker('refresh');
    });
}

function limpiar() {
    $("#idproveedor").val("");
    $("#proveedor").val("");
    $("#serie_comprobante").val("");
    $("#num_comprobante").val("");
    $("#impuesto").val("0");

    $("#total_compra").val("");
    $(".filas").remove();
    $("#total").html("0");

    let now = new Date();
    let day = ("0" + now.getDate()).slice(-2);
    let month = ("0" + (now.getMonth()+1)).slice(-2);
    let today = now.getFullYear()+"-"+(month)+"-"+(day);
    $('#fecha_hora').val(today);

    $("#tipo_comprobante").val("Nota de Gastos");
    $("#tipo_comprobante").selectpicker('refresh');
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoregistros").hide();
        $("#formularioregistros").show();
        // $("#btnGuardar").prop("disabled", false);
        $("#btnagregar1").hide();
        listarArticulos();

        $("#btnGuardar").hide();
        $("#btnCancelar").show();
        detalles=0;
        $("#btnAgregarArt").show();

    }else {
        $("#listadoregistros").show();
        $("#formularioregistros").hide();
        $("#btnagregar1").show();
    }
}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla = $('#tbllistado').dataTable ({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: '../ajax/ingreso.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 5, //paginacion
        "order": [[0, "des"]] //ordenar
    }).DataTable();
}

function listarArticulos() {
    tabla = $('#tblarticulos').dataTable ({
        "aProcessing": true, //procesamiento del datatables
        "aServerSide": true, //paginación y filtrado realizados por el servidor
        dom: 'Bfrtip',
        buttons: [
        ],
        "ajax": {
            url: '../ajax/ingreso.php?op=listarArticulos',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 5, //paginacion
        "order": [[0, "des"]] //ordenar
    }).DataTable();
}

function guardaryeditar(e) {
    e.preventDefault();
    // $("#btnGuardar").prop("disabled", true);
    let formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: "../ajax/ingreso.php?op=guardaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function(datos) {
            bootbox.alert(datos);
            mostrarform(false);
            listar();
        }
    });
    limpiar();
}

function mostrar(idingreso) {
    $.post("../ajax/ingreso.php?op=mostrar",{idingreso : idingreso}, function(data, status) {
        data = JSON.parse(data);
        mostrarform(true);

        $("#idproveedor").val(data.idproveedor);
        $("#idproveedor").selectpicker('refresh');
        $("#tipo_comprobante").val(data.tipo_comprobante);
        $("#tipo_comprobante").selectpicker('refresh');
        $('#serie_comprobante').val(data.serie_comprobante);
        $("#num_comprobante").val(data.num_comprobante);
        $("#fecha_hora").val(data.fecha);
        $("#impuesto").val(data.impuesto);
        $("#idingreso").val(data.idingreso);
        //ocultar y mostrar los botones

        $("#btnGuardar").hide();
        $("#btnCancelar").show();
        $("#btnAgregarArt").hide();
    });
    $.post("../ajax/ingreso.php?op=listarDetalle&id="+idingreso, function(r) {
        $("#detalles").html(r);
    });
}

function anular(idingreso) {
    bootbox.confirm("¿Esta seguro de anular el documento?", function (result) {
        if (result) {
            $.post("../ajax/ingreso.php?op=anular", {idingreso : idingreso}, function (e) {
                bootbox.alert(e);
                tabla.ajax.reload();
            });
        }
    })
}

//variables necesarias para trabajar con las compras y sus detalles
const impuesto=21;
let cont=0;
let detalles=0;
// $("#guardar").hide();
$("#btnGuardar").hide();
$("#tipo_comprobante").change(marcarImpuesto);

function marcarImpuesto() {
   const tipo_comprobante=$("#tipo_comprobante option:selected").text();
   if(tipo_comprobante==='Factura') {
       $('#impuesto').val(impuesto);
   }else {
       $('#impuesto').val("0");
   }
}

function agregarDetalle(idarticulo, articulo) {
    const cantidad=1;
    const precio_compra=1;
    const precio_venta=1;

    if (idarticulo !== "") {
        const subtotal=cantidad*precio_compra;
        const fila = '<tr class="filas" id="fila'+cont+'">' +
            '<td><button type="button" class="btn btn-danger" onclick="eliminarDetalle('+cont+')">X</button></td>'+
            '<td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td>'+
            '<td><input type="number" name="cantidad[]" id="cantidad[]" value="'+cantidad+'"></td>'+
            '<td><input type="number" name="precio_compra[]" id="precio_compra[]" value="'+precio_compra+'"></td>'+
            '<td><input type="number" name="precio_venta[]" value="'+precio_venta+'"></td>'+
            '<td><span name="subtotal" id="subtotal'+cont+'">'+subtotal+'</span></td>'+
            '<td><button type="button" onclick="modificarSubtotales()" class="btn btn-info"><i class="fa fa-refresh"></i></button></td>'+
            '</tr>';
            cont++;
            detalles += 1;
            $('#detalles').append(fila);
            modificarSubtotales();
    } else {
        box.alert("Error al ingresar el detalle");
    }
}

function modificarSubtotales() {
    let cant = document.getElementsByName("cantidad[]");
    let prec = document.getElementsByName("precio_compra[]");
    let sub = document.getElementsByName("subtotal");

    for (let i = 0; i < cant.length; i++) {
        let inpC = cant[i];
        let inpP = prec[i];
        let inpS = sub[i];

        let Subtl;
        Subtl = inpS.value = inpC.value * inpP.value;
        document.getElementsByName("subtotal")[i].innerHTML = Subtl.toFixed(2);
        // inpS.value = inpC.value * inpP.value;
        // document.getElementsByName("subtotal")[i].innerHTML = inpS.value;
    }
    calcularTotales();
}

function calcularTotales() {
    let sub = document.getElementsByName("subtotal");
    let total = 0.00;

    for (let i = 0; i < sub.length; i++) {
        total += document.getElementsByName("subtotal")[i].value;
    }
    $("#total").html("€/. " + total);
    $("#total_compra").val(total);
    evaluar();
}

function evaluar() {
    if (detalles>0) {
        $("#btnGuardar").show();
    } else {
        $("#btnGuardar").hide();
        cont=0;
    }
}

function eliminarDetalle(indice) {
    $("#fila" + indice).remove();
    calcularTotales();
    detalles=detalles-1;
}

init();
