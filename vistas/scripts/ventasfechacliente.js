let tabla;

function init() {
    listar();

    $.post("../ajax/venta.php?op=selectCliente", function(r){
        $("#idcliente").html(r);
        $('#idcliente').selectpicker('refresh');
    });
}

function listar() {
    let fecha_inicio = $("#fecha_inicio").val();
    let fecha_fin=$("#fecha_fin").val();
    let idcliente=$("#idcliente").val();

    tabla = $('#tbllistado').dataTable ({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: '../ajax/consultas.php?op=ventasfechacliente',
            data:{fecha_inicio: fecha_inicio, fecha_fin: fecha_fin, idcliente: idcliente},
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 5, //paginacion
        "order": [[0, "des"]] //ordenar

    }).DataTable();
}

init();
