<?php
/**
 * Created by PhpStorm.
 * User: davidsolanes
 * Date: 15/11/2018
 * Time: 21:47
 */

 require_once "../config/Conexion.php";

 class Permiso {

    public function __construct() {

    }

    public function listar() {
        $sql = "SELECT * FROM permiso";
        return ejecutarConsulta($sql);
    }

}